<!--
If you see this comment, you are reading the wrong file!
View #filename(instructions.pdf) or #(instructions.html)
per instructions in #filename(README.txt).
-->

---
author: #module_leader
date: #date
documentclass: hitec
fontfamily: opensans
fontsize: 12
...
# Overview

This deliverable is the Research Question component of the
coursework.  You will submit your research question using Git, so it
is important that you understand how to use Git to commit and upload
files to #githost from the command line.  Using the #githost web
interface is not acceptable and will result in reduced marks for the
"Research Infrastructure" component  of the coursework.

To submit the Research Question deliverable successfully, you must complete three steps:

1. Identify a dataset.
2. Formulate a research question, in one of the three forms specified in the Research Questions lecture.
3. Document and submit your research question, using Git.

Each of these steps is described in detail below.

# Instructions

## Identify a dataset.

#. Read the research question section of the  coursework
 specification, to be sure you understand the requirements for this
 part of the coursework.

#. Discuss potential research topics among yourselves, and decide on some general topics of interest to the group, such as "cricket", "COVID-19", "happiness", "crime", "employment", etc. 

#. Visit [#dataset_site_name](#dataset_site) and register for a free (community) account.

#. Login to [#dataset_site_name](#dataset_site).

#. Search for datasets relevant to your topic.

#. Choose one to three candidate datasets.

## Formulate a Research Question

#. Look at the columns in your dataset.  These are the variables you
 will use when formulating your research question.

#. Identify the *meaning* of the variable names.  This will help you
 formulate a research question.

#. Determine which variable will be your _independent_ variable (the
 one that determines the outcome), and which variable will be your
 _dependent_ variable (the one that represents the _value_ of the outcome).

#. Identify the *kind* of data represented by the independent and dependent variables:
 *interval*, *ordinal*, or *nominal*.  This will determine what kind
 of analysis you can do:

    * If both are interval or ordinal, you may use correlation (but
      only if the independent variable has at least eight distinct
      values).

    * If the _independent_ variable is nominal, or is ordinal with less
      than eight (8) values, but the _dependent_ variable is interval, you
      may use comparison of _means_ (or medians).

    * If the _independent_ variable is nominal, or is ordinal with less
      than eight (8) values, and the _dependent_ variable is an interval
      value representing a proportion (fraction or percentage), you
      should use comparison of _proportions_.

    The Research Questions lecture recordings and notes discuss these
    differences in some detail.  Also, you might want to read the
    marking rubric (#filename(rubric.xlsx)) to see how we will mark
    your research question deliverable.





## Document and Submit your Research Question


#. If you haven't already done so, clone your group's #githost repository into a local workspace.

#. Change directories to the directory of your local workspace.

#. Configure your email address.  From the command line, type

        git config user.email abc21def@herts.ac.uk

    Be sure to user _your_ Herts email address, so your contributions
    to your project are recorded.

### Edit #filename(research_question.yml)

**CAUTION** #filename(research_question.yml) is a "semi-structured"
  file in YAML format.  YAML is a data serialization language that
  uses key-value pairs, similar to those in Windows "INI" files.

Pay careful attention to whitespace, spelling, capitalization, and structure in the instructions below.

#. Copy #filename(research_question.yml) from this instructions workspace to your group workspace.

#. Change to your group workspace, and open #filename(research_question.yml)
 (using Notepad++ or your favorite text editor.  Do  **NOT** use  Windows Notepad!).

#. Write your group name after the `group: ` key.

#. Write the names (but **NOT** student IDs) of each group member,
separated by a comma (','), 
within the square brackets after the `members: ` key.

#. Write the general topic area from which you derived your research
 question after the `topic: ` key.

#. Write your research question after the `RQ: ` key.  Be sure it
 is an actual _question_, written following one of the three question
 templates specified in the Research Question lecture recording and
 notes.

    Note: groups who do not express their research question using one of the three question templates
    will fail this assignment.

#. Write the null hypothesis for your research question after the
`null-hypothesis: ` key.

#. Write the alternative hypothesis for your research question after the
`alt-hypothesis: ` key.

#. Write the URL of your dataset after the `dataset-url: ` key.
This *must* be a URL from #dataset_site, and have #dataset_host in the
hostname portion of the URL.

#. Download your dataset into your Git workspace, and add it to the
 staged commit using #command(git add DATASET.csv) (replace
 "DATASET" with the actual filename of your dataset).

#. Load your dataset into R using the #command(read.csv()) function:

        > d <- read.csv("DATASET.csv")

    (Replace "DATASET" with the actual name of your dataset file).

#. Print the column names of your dataset using the #command(colnames)
 function:

        > colnames(d)

#. Copy the command invocation and its output, and paste it into the space under the
 `columns: |` key.

#. Insert *two* spaces before every line in the output you pasted into the space under the
 `columns: |` key.

#. Proofread your file to be sure you have spelled everything
correctly, that you have expressed your research question as a
question, and that you have followed YAML syntax (Wikipedia has a nice
summary of [YAML syntax](https://en.wikipedia.org/wiki/YAML) if you
are curious).

#. Commit your changes using #command(git commit -m "COMMIT LOG MESSAGE")  (replace "COMMIT LOG MESSAGE" with an actual,
_meaningful_ commit log message).

#. Push your workspace to #githost by the deadline (23:59 #cw_rq_due).

# Notes


#. YAML is a _data serialization_ language.  As such, the syntax of
   the #filename(research_question.yml) file is important:

    * Keys must be spelled *exactly* as shown, with *exactly* the same
      capitalization and spelling.
    * All keys *must* be followed by a colon (':') then a space.
    * The vertical bar after the `columns` key is meaningful: it means
      the indented text that follows should be formatted exactly as
      written.
    * You *must* indent the text after the `columns` key by two spaces.

#. Groups who push their #filename(research_question.yml) file the
day before the deadline will get feedback about whether their file
follows correct YAML syntax.

#. There are only _three_ acceptable formats for research questions in
 *this module* (7COM1085--Team Research and Development Project).
 These are described in detail in the Research Questions lecture
 recording and accompanying notes.

    Be sure you  understand the three formats *before* you write your
research question.

#. The marking rubric (#filename(rubric.xlsx)) shows how we will mark
 your #filename(research_question.yml); test yourself before submitting.


# Example

The following example asks about the relationship between happiness
and generosity (do NOT use the same question for your deliverable!).

~~~~~
#include(example.yml)
~~~~~
